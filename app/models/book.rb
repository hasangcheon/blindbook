class Book < ActiveRecord::Base
  belongs_to :user
  has_many :meetings

  acts_as_taggable

  def available?
  	meetings.where(end_datetime: nil).empty?
  end
end
