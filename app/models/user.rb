class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

 	has_many :books
 	has_many :meetings

 	def meeting_books
 		meetings.where(end_datetime: nil)
 	end
end
